﻿using HW40.Models;
using System;

namespace HW40
{
    class Program
    {
        static void Main(string[] args)
        {

            //чтоб база данных создалась
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var user1 = new User()
                {
                    Login = "baku",
                    FirstName ="Baktybek",
                    SurName = "Jumabekov",
                    DateOfBirth = new DateTime(2005,06,02),
                    AccountCreationDate = DateTime.Now
                };
                db.Users.Add(user1);
                db.SaveChanges();

                var publication = new Publication()
                {
                    UserID = user1.ID,
                    Comment = "Была сделана оффигенная публикация"
                }; 
                var publication2 = new Publication()
                {
                    UserID = user1.ID,
                    Comment = "Была сделана еще оффигенная публикация"
                };

                db.Publications.Add(publication);
                db.Publications.Add(publication2);  
                db.SaveChanges();
            }
        }
    }
}
