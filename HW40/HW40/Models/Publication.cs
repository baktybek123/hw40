﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW40.Models
{
   public class Publication
    {
        public int ID { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        public string Comment { get; set; }
        public string Photo { get; set; }
        public string AddressPublication { get; set; }
        public virtual User User { get; set; }
        
    }
}
