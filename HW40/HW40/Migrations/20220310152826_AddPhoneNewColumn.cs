﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HW40.Migrations
{
    public partial class AddPhoneNewColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddressPublication",
                table: "Publications",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Photo",
                table: "Publications",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressPublication",
                table: "Publications");

            migrationBuilder.DropColumn(
                name: "Photo",
                table: "Publications");
        }
    }
}
