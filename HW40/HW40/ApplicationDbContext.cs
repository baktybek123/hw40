﻿using HW40.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW40
{
   public class ApplicationDbContext : DbContext 
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Publication> Publications { get; set; }

        //public ApplicationDbContext()
        //{
        //    Database.EnsureCreated();
        //}

        protected override void  OnConfiguring(DbContextOptionsBuilder optionsBuilder)
       {
            var build = new ConfigurationBuilder();
            build.SetBasePath(@"C:\GitRepository\hw40\HW40\HW40");
            build.AddJsonFile("appsetings.json");
            var config= build.Build();
            string connectionstring = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionstring);
       }


    }
}
